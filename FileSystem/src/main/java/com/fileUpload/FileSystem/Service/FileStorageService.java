package com.fileUpload.FileSystem.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.xml.MarshallingHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.reactive.function.client.WebClient;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fileUpload.FileSystem.Configuration.FileStorageProperties;
import com.fileUpload.FileSystem.Exception.FileNotFoundException;
import com.fileUpload.FileSystem.Exception.FileStorageException;
import com.fileUpload.FileSystem.Model.UserDetails;
import com.fileUpload.FileSystem.Repository.UserDetailsRepository;
import com.google.gson.Gson;
import com.kuliza.Assignment.Model.Employee;
import com.kuliza.Assignment.Pojo.EmployeePojo;
import com.kuliza.Assignment.Pojo.UsersPojo;
import com.mysql.cj.xdevapi.JsonArray;
import com.mysql.cj.xdevapi.JsonValue;

@Service
public class FileStorageService {

	private final Path fileStorageLocation;

	@Autowired
	private UserDetailsRepository userRepo;

	@Autowired
	FileStorageProperties fsp;

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	private WebClient.Builder webClientBuilder;

	@Autowired
	public FileStorageService(FileStorageProperties fileStorageProperties) {
		this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir()).toAbsolutePath().normalize();

		try {
			Files.createDirectories(this.fileStorageLocation);
		} catch (Exception ex) {
			throw new FileStorageException("Could not create the directory where the uploaded files will be stored.",
					ex);
		}

	}

	public String storeFile(MultipartFile file) {

		String fileName = StringUtils.cleanPath(file.getOriginalFilename());

		try {
			if (fileName.contains("..")) {
				System.out.println("Invalid File name");
				throw new FileStorageException("Sorry fileName contains Invalid path sequence" + fileName);
			}

			Path targetLocation = this.fileStorageLocation.resolve(fileName);
			Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

			FileInputStream fis = new FileInputStream(new File(fsp.getUploadDir() + "/" + fileName));

			System.out.println(fsp.getUploadDir() + fileName);

			// EmployeePojo p= new EmployeePojo("Rss", "26", 24000, "9887992211");

			// Employee le= restTemplate.getForObject("http://localhost:8080//getemp/1",
			// Employee.class);

			// System.out.println(le.toString());

			// restTemplate.postForObject("http://localhost:8080/", p, String.class);
			List<HashMap<String, String>> address = new ArrayList<>();

//			HashMap<String, String> h1 = new HashMap<>();
		
//			address.add(h1);
			
			
			JSONArray a = new JSONArray();
			JSONObject h1 = new JSONObject();
			h1.put("streetname", "TonkRoad");
			h1.put("statename", "Rajasthan");
			h1.put("pincode", "302015");
			h1.put("cityname", "Jaipur");
			a.put(h1);

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);

			//List<HttpMessageConverter<?>> msgcnvrt = restTemplate.getMessageConverters();

//			MappingJackson2HttpMessageConverter jmc= new MappingJackson2HttpMessageConverter();
//			XStreamMarshaller marshaller = new XStreamMarshaller();
//		    MarshallingHttpMessageConverter marshallingConverter = new MarshallingHttpMessageConverter(marshaller);
//			messageConverterList.add(jsonMessageConverter);
//			messageConverterList.add(marshallingConverter);
//			restTemplate.setMessageConverters(messageConverterList);

//			List<String> l1 = new ArrayList<>();
//
//			l1.add("Piyush");

			// UsersPojo p = new UsersPojo("AArjav", "9887122334", "25", 24000, address);

			// headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

//			 JsonArray jArray= new JsonArray();
			// headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			
			
			JSONObject jo = new JSONObject();

			jo.put("name", "AaravJain");
			jo.put("phone", "9024776560");
			jo.put("age", "25");
			jo.put("salary", 25000);
			 jo.put("address", a);

		//	jo.put("list", l1);

//			ObjectMapper mapper = new ObjectMapper();
//			mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
//			String s = mapper.writeValueAsString(h1);

			
			// List<address> l1= new ArrayList<>();
			// String javaObject= mapper.writeValueAsString(jo);
			// restTemplate.postForObject("http://localhost:8080/postuser", jo,
			// String.class);
			// System.out.println(jo.get("address"));

			// HttpEntity<List<String>> entity = new HttpEntity<List<String>>(l1,headers);

			// HttpEntity<HashMap<String,String>> ent= new
			// HttpEntity<HashMap<String,String>>(h1,headers);
		//	Gson g = new Gson();

			// String joString= g.toJson(jo);

		//	String j = jo.toString();

			HttpEntity<String> entity = new HttpEntity<String>(jo.toString(), headers);
			
			System.out.println(entity.getBody());

			// HttpEntity<JsonArray> ent= new HttpEntity<JsonArray>(jArray,headers);
			// System.out.println(jo.toString());

			// restTemplate.getMessageConverters().add(new
			// MappingJackson2HttpMessageConverter());
			// restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
			// System.out.println(ent.toString());
			// restTemplate.postForObject("http://localhost:8080/postadd", entity,
			// String.class);
			// restTemplate.postForObject("http://localhost:8080/postuser", entity,
			// String.class);

			restTemplate.postForEntity("http://localhost:8080/postUser", entity, String.class);

			@SuppressWarnings("resource")
			XSSFWorkbook workbook = new XSSFWorkbook(fis);

			XSSFSheet sheet = workbook.getSheetAt(0);

			// Row row;

			// Cell cell;

			for (int i = 1; i <= sheet.getLastRowNum(); i++) {

				// double data=sheet.getRow(1).getCell(3).getNumericCellValue();
				// System.out.println("Hello"+data);
				// row= sheet.getRow(i);
				// cell=row.createCell(i);

				// int j= row.getLastCellNum()
				// System.out.println(cell.getNumericCellValue());

				UserDetails user = new UserDetails();

				if (sheet.getRow(i).getCell(0).getCellType() != null
						&& sheet.getRow(i).getCell(1).getCellType() != null) {

				}

				try {
					System.out.println("--------------------Cell Type :" + sheet.getRow(i).getCell(0).getCellType());

					user.setName(sheet.getRow(i).getCell(0).getStringCellValue());
				} catch (IllegalStateException e) {

					System.out.println("Can not save this .-------------- ");
					Double name = sheet.getRow(i).getCell(0).getNumericCellValue();
					String name1 = Double.toString(name);
					user.setName(name1);
				}

				try {
					user.setAge((int) sheet.getRow(i).getCell(1).getNumericCellValue());
				} catch (IllegalStateException e) {
					// e.printStackTrace();
				}
				try {
					user.setPhone((long) sheet.getRow(i).getCell(2).getNumericCellValue());
				} catch (IllegalStateException e) {
					// e.printStackTrace();
				}
				try {
					user.setSalary((int) sheet.getRow(i).getCell(3).getNumericCellValue());
				} catch (IllegalStateException e) {
					// e.printStackTrace();

				}

				userRepo.save(user);

			}

//			Iterator<Row> rowIterator=sheet.iterator();
//			
//			while(rowIterator.hasNext())
//			{
//				Row row = rowIterator.next();
//				
//				
//				Iterator<Cell> cellIterator= row.cellIterator();
//				
//					while(cellIterator.hasNext()) {
//					
//					Cell cell = cellIterator.next();
//					}
//				
//			}
			return fileName;

		} catch (IOException ex) {
			throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);

		} catch (IllegalStateException ex) {
			throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return fileName;

	}

	public Resource loadFileAsResource(String fileName) {
		try {
			Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
			Resource resource = new UrlResource(filePath.toUri());
			if (resource.exists()) {
				return resource;
			} else {
				throw new FileNotFoundException("File not found " + fileName);
			}
		} catch (MalformedURLException ex) {
			throw new FileNotFoundException("File not found " + fileName, ex);
		}

	}

	@SuppressWarnings("resource")
	public void readExcel() throws Exception {

//		
//		FileInputStream fis= new FileInputStream(new File("./src/main/uploads/Spreadsheet.xlsx"));
//
//		XSSFWorkbook workbook= new XSSFWorkbook(fis);
//		
//		XSSFSheet sheet= workbook.getSheetAt(0);
//		
//		Row row;
//		System.out.println("Piyush Jain"+sheet.getLastRowNum());
//		for(int i=1;i<=sheet.getLastRowNum();i++)
//		{
//			row= sheet.getRow(i);
//			UserDetails user= new UserDetails();
//			int j=0;
//				user.setName(row.getCell(j).toString());
//				user.setAge(row.getCell(j+1).toString());
//				user.setPhone(row.getCell(j+2).toString());
//				user.setSalary(row.getCell(j+3).toString());
//				userRepo.save(user);
//			
//			

	}

}
