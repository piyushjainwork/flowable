package com.kuliza.Assignment.Pojo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class UsersPojo {
	
	private String name;
	
	
	private String phone;
	
	
	private String age;
	
	
	private int salary;
	
	private List<String> l1=new ArrayList<>();
	
	
	
	public List<String> getL1() {
		return l1;
	}

	public void setL1(List<String> l1) {
		this.l1 = l1;
	}

	public UsersPojo(@Size(min = 3) String name, @Size(min = 10, max = 10) String phone, @NotBlank String age,
			int salary, List<HashMap<String, String>> address) {
		super();
		this.name = name;
		this.phone = phone;
		this.age = age;
		this.salary = salary;
		this.address = address;
	}

	private List<HashMap<String,String>> address= new ArrayList<>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	public List<HashMap<String, String>> getAddress() {
		return address;
	}

	public void setAddress(List<HashMap<String, String>> address) {
		this.address = address;
	}
	
	
	
	

}
